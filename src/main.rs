
// use crate::observability;

mod observability;
mod error;
use axum::{response::Html, routing::get, Router};
use tracing::debug;


#[tokio::main]
async fn main() -> error::Result<()> {

    // Set up a tracing subscriber with JSON formatting
    tracing_log::LogTracer::builder()
        .ignore_crate("sqlx")
        .with_max_level(log::LevelFilter::Info)
        .init()
        .expect("could not initialize log tracer");

    match observability::configure_observability().await {
        Ok(_) => {
            tracing::debug!("tracing configured");
        }
        Err(err) => {
            tracing::error!("error configuring tracing: {}", err);
            return Err(err);
        }
    };

    // build our application with a route
    let app = Router::new().route("/", get(handler));

    // run it
    debug!("listening on 0.0.0.0:3000");

    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
    .serve(app.into_make_service())
    .await
    .expect("server failed");

    Ok(())
}

#[tracing::instrument]
async fn handler() -> Html<&'static str> {
    Html("<h1>Hello, World!</h1>")
}
