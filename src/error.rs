use opentelemetry::trace::TraceError;

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    TraceErr(String),
    SetGlobalDefaultErr(String)
}

impl From<tracing::subscriber::SetGlobalDefaultError> for Error {
    fn from(_: tracing::subscriber::SetGlobalDefaultError) -> Self {
		Error::SetGlobalDefaultErr("Set global err".to_string())
	}
}

impl From<TraceError> for Error {
    fn from(_: TraceError) -> Self {
		Error::TraceErr("Trace err".to_string())
	}
}
impl core::fmt::Display for Error {
	fn fmt(
		&self,
		fmt: &mut core::fmt::Formatter,
	) -> core::result::Result<(), core::fmt::Error> {
		write!(fmt, "{self:?}")
	}
}