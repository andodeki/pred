use opentelemetry_otlp::WithExportConfig;
use tracing_bunyan_formatter::{BunyanFormattingLayer, JsonStorageLayer};
use tracing_subscriber::prelude::__tracing_subscriber_SubscriberExt;
use crate::error::Result;
use tracing::debug;

const OTEL_EXPORTER_OTLP_ENDPOINT_ENV_VAR: &str = "OTEL_EXPORTER_OTLP_ENDPOINT";
const OTEL_EXPORTER_OTLP_ENDPOINT_DEFAULT: &str = "http://localhost:4317";

const OBSERVABILITY_SERVICE_NAME_ENV_VAR: &str = "OBSERVABILITY_SERVICE_NAME";
const OBSERVABILITY_SERVICE_NAME_DEFAULT: &str = "pred";

#[tracing::instrument]
pub async fn configure_observability() -> Result<()> {
    debug!("{:<12} - configure_observability", "fn");

    let otel_exporter_endpoint =
        dotenvy::var(OTEL_EXPORTER_OTLP_ENDPOINT_ENV_VAR).unwrap_or_else(|_| {
            tracing::warn!(
                "{} Env var not set, using default",
                OTEL_EXPORTER_OTLP_ENDPOINT_DEFAULT
            );
            OTEL_EXPORTER_OTLP_ENDPOINT_DEFAULT.to_string()
        });

    let observability_service_name = dotenvy::var(OBSERVABILITY_SERVICE_NAME_ENV_VAR)
        .unwrap_or_else(|_| OBSERVABILITY_SERVICE_NAME_DEFAULT.to_string());

    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint(otel_exporter_endpoint),
        )
        .with_trace_config(opentelemetry::sdk::trace::config().with_resource(
            opentelemetry::sdk::Resource::new(vec![opentelemetry::KeyValue::new(
                "service.name",
                observability_service_name.clone(),
            )]),
        ))
        .install_batch(opentelemetry::runtime::Tokio)?;

    // Create a tracing layer with the configured tracer
    let telemetry_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    let filter = tracing_subscriber::EnvFilter::from_default_env();

    cfg_if::cfg_if! {
        if #[cfg(feature="bunyan")] {
                // Create a new formatting layer to print bunyan formatted logs to stdout, pipe into bunyan to view
                let formatting_layer = BunyanFormattingLayer::new(observability_service_name, std::io::stdout);
                let subscriber = tracing_subscriber::Registry::default()
                    .with(filter)
                    .with(telemetry_layer)
                    .with(JsonStorageLayer)
                    .with(formatting_layer);
        } else {
                let subscriber = tracing_subscriber::Registry::default()
                .with_filter(filter)
                .with_writer(std::io::stdout)
                .with(telemetry_layer);
            }
    }

    Ok(tracing::subscriber::set_global_default(subscriber)?)
}

